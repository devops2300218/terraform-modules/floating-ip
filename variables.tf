variable "ext_pool" {
  description = "Poll for external IPs"
  type        = string
  default     = "Ext-Net"
}
variable "instance_id" {
  description = "ID of the Instance"
  type        = string
}
variable "region" {
  description = "The OVH Region"
  type        = string
}
