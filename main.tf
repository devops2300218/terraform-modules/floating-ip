resource "openstack_networking_floatingip_v2" "this" {
  pool   = var.ext_pool
  region = var.region
}

resource "openstack_compute_floatingip_associate_v2" "this" {
  floating_ip = openstack_networking_floatingip_v2.this.address
  region      = var.region
  instance_id = var.instance_id
}
